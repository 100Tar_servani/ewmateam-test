import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue';
import Movie from '../views/Movie.vue';

Vue.use(VueRouter)

let router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/:page?',
			name: 'home',
			component: Home
		},
		{
			path: '/movie/:movieId/:page?',
			name: 'movie',
			component: Movie
		},
	]
});

export default router;